import json
import time
import random
import logging
from datetime import datetime, timedelta

def get_timestamp():
    return datetime.timestamp(datetime.now())

def get_timestamp_str():
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S")

def job_queue_callback(job_object):
    logger = logging.getLogger("uvicorn")

    from pprint import pprint
    print(f"Job {job_object.id} callback. Payload:")
    pprint(job_object.to_dict(munge=False))

    

    # if not bool(random.randint(0,1)):
    #     raise ValueError("You gotta try harder than that!")

    run_parts = ["step1", "step2", "step3", "step4"]
    for part in run_parts:
        logger.debug(f"Running Job part: {part}")
        time.sleep(1)
        job_object.add_job_log(
            {
              "name": part,
              "status": "successful",
              "response": f"Did the processing for: {part}"
            }
        )
    return True

def is_json(received_data):
    try:
        json.loads(received_data)
    except ValueError as e:
        return False
    return True

