import logging
from ..jobqueue import Job
from .._functions import *
from .._classes import *
from .plugins import available_plugins


class JobLog():
    def __init__(self, plugin="pickledb") -> None:
        self.logger = logging.getLogger("uvicorn")

        # Load the engine/plugin
        if not plugin in available_plugins:
            self.logger.warning(f"Log engine: '{plugin}' not found. Using default plugin: 'pickledb'")
            plugin = "pickledb"
        self.plugin = available_plugins[plugin].module
        self.logger.debug(f"Using the '{self.plugin.name}' engine for JobLogs")

    def exists(self, job_id: str):
        return self.plugin.exists(job_id)
    
    def save(self, job_id: str, job_dict: dict):
        self.plugin.save(job_id, job_dict)

    def update(self, job_object):
        self.plugin.update(job_object.id, job_object.to_dict())

    def delete(self, job_id: str):
        self.plugin.delete(job_id)

    def get(self, job_id: str):
        outjob = Job()
        job_dict = self.plugin.get(job_id)
        if job_dict:
            outjob.from_dict(job_dict)
            return outjob
        else:
            return None
    
    def get_all(self, munge: bool=False):
        all_jobs = self.plugin.get_all()
        if not munge:
            out_jobs = all_jobs
        else:
            out_jobs = []
            for job_dict in all_jobs:
                outjob = Job()
                outjob.from_dict(job_dict)
                job_dict = outjob.to_dict(munge=True)
                out_jobs.append(job_dict)
        return {"total": len(all_jobs), "jobs": out_jobs}
    
    def get_incomplete(self):
        ret_jobs = []
        filter = QueryFilter(
            field="metadata.status",
            values=[Job.STATUS_QUEUED, Job.STATUS_RUNNING, Job.STATUS_RETRY],
            operator="=="
        )
        # Convert the return dict to a Job object
        for job_dict in self.plugin.query(filters=[filter]):
            outjob = Job()
            outjob.from_dict(job_dict)
            ret_jobs.append(outjob)
        
        return ret_jobs

    def prune(self, job_id: str=None, hours: int=None, all: bool=False, in_queue: list=[]):
        if all:
            self.logger.info(f"Pruning all JobLogs")
            del_count = self.plugin.prune(all=True, in_queue=in_queue)
            return {"message": f"Pruned {del_count} JobLogs"}
        elif hours:
            self.logger.info(f"Pruning JobLogs older than {hours} hours")
            del_count = self.plugin.prune(hours=hours, in_queue=in_queue)
            return {"message": f"Pruned {del_count} JobLogs older than {hours} hours"}
        elif job_id:
            self.logger.info(f"Pruning JobLog for job id: {job_id}")
            if self.plugin.prune(job_id=job_id, in_queue=in_queue) > 0:
                return {"message": f"JobLog for job id {job_id} pruned"}
            else:
                return {"message": f"JobLog for job id {job_id} not found"}
