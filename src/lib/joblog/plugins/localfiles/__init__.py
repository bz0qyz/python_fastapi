import os
import json
import time
import logging
import pickledb


class JobLogPlugin():
    def __init__(self, db_file=None) -> None:
        self.name = "pickledb"
        self.logger = logging.getLogger("uvicorn")
        db_dir = f"{os.path.dirname(__file__)}{os.sep}data"
        if not os.path.exists(db_dir):
            os.makedirs(db_dir)
        if db_file is None:
            self.db_file = os.path.join(db_dir, "joblog.db")
        else:
            self.db_file = db_file
        self.logger.info(f"Loading {self.name} data file: {db_file}")
        self.db = pickledb.load(self.db_file, True)
        
    def exists(self, job_id: str):
        return self.db.exists(job_id)

    def save(self, job_id: str, job_dict: dict):
        if isinstance(job_dict, dict):
            job_dict = json.dumps(job_dict)
        self.db.set(job_id, job_dict)
        self.db.dump()

    def update(self, job_id: str, job_dict: dict):
        self.db.set(job_id, json.dumps(job_dict))

    def delete(self, job_id: str):
        self.db.rem(job_id)
        self.db.dump()

    def get(self, job_id: str):
        self.logger.debug(f"Getting log for job {job_id}")
        job_dict = self.db.get(job_id)
        if job_dict:
            return json.loads(job_dict)
        else:
            return None
    
    def get_all(self):
        all_jobs = []
        all_keys = self.db.getall()
        for jkey in all_keys:
            all_jobs.append(self.get(jkey))
        return all_jobs
    
    def prune(self, job_id: str=None, hours: int=None, all: bool=False, in_queue: list=[]):
        del_keys = []

        if job_id:
            del_keys.append(job_id)
        elif hours or all:
            for jkey in self.db.getall():
                job_dict = self.get(jkey) or None
                if job_dict:
                    if all:
                        del_keys.append(jkey)
                    else:
                        if not "metadata" in job_dict or not "timestamp" in job_dict["metadata"]:
                            continue
                        if job_dict["metadata"]["timestamp"] < (time.time() - (hours * 3600)):
                            if not jkey in in_queue:
                                del_keys.append(jkey)
        elif job_id and not job_id in in_queue:
            del_keys.append(job_id)
        
        # Delete the keys
        for jkey in del_keys:
            self.delete(job_id=jkey)

        return len(del_keys)
    
    def query(self, filters: list):
        """
        Filters is a list of QueryFilter objects
        """
        from pprint import pprint
        ret_jobs = []

        all_keys = self.db.getall()
        for jkey in all_keys:
            passed = False
            for filter in filters:
                passed = self.__query_pass__(self.get(jkey), filter)
            if passed:
                ret_jobs.append(self.get(jkey))

        return ret_jobs
    
    def __query_pass__(self, row: dict, filter):
        """ Filter is a QueryFilter object """
        all_passed = False
        field_passed = False
        value_passed = False

        # Verify that the field exists in the row
        if len(filter.field) == 0 and filter.field[0] in row:
            field_passed = True
        elif len(filter.field) > 1 and filter.field[1] in row[filter.field[0]]:
            field_passed = True
        
        # Verify that the value matches the filter
        if field_passed and len(filter.field) == 0:
            if row[filter.field[0]] in filter.values:
                value_passed = True
        elif field_passed and len(filter.field) > 1:
            if row[filter.field[0]][filter.field[1]] in filter.values:
                value_passed = True

        if filter.operator == "!=":
            value_passed = not value_passed

        if field_passed and value_passed:
            all_passed = True
        return all_passed