import json
from typing import List, Union, Dict
from datetime import datetime
from pydantic import BaseModel

class BasicResponse(BaseModel):
    """ Basic Response """
    status: str
    message: str
    def __str__(self) -> str:
        return json.dumps(self.dict())

class QueueSize(BaseModel):
        total: int
        in_progress: int

class QueueStatus(BaseModel):
    """ Queue Status """
    jobs: List[str] = []
    size: QueueSize

class AppInfo(BaseModel):
    """ App Info """
    name: str
    version: str
    description: str
    log_level: str
    queue: QueueStatus
    backends: Dict[str, str]

class JobAttemptLog(BaseModel):
    info: str = None
    error: str = None
    time: datetime
    attempt: int

class JobPayload(BaseModel):
    """ Payload for a Job """
    name: str
    message: str = None
    apikey: str = None
    notify: Dict = {}
    
class JobMetadata(BaseModel):
    """ Metadata for a Job """
    id: str
    type: str
    attempts: int
    status: str
    timestamp: int
    time_created: str
    time_started: str = None
    time_completed: str = None
    completed: bool
    
class JobItem(BaseModel):
    """ Job Item """
    metadata: JobMetadata
    job_log: List[Dict]
    attempt_log: List[JobAttemptLog]
    payload: JobPayload
    
class JobList(BaseModel):
    """ List of Jobs """
    total: int
    jobs: List[JobItem]

class NotifyEmail(BaseModel):
    recipients: List[str]