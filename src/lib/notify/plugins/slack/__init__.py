import os
import logging
import json
import requests
from jinja2 import Environment, FileSystemLoader

class NotificationPlugin(object):
    def __init__(self, **backend_config):
        # self.template_dir = template_dir
        self.logger = logging.getLogger(__name__)
        for key, value in backend_config.items():
            setattr(self, key, value)

    def send(self, message):
        """
        Send a notifaction with Slack
        """
        self.logger.info("Sending %s Notification", self.type)
        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json"
        }

        header_fields = []
        for field in message["header_fields"]:
            header_fields.append(
                {'type': 'mrkdwn', 'text': "*{}:* {}".format(field["label"], field["value"])}
            )

        request_payload = {
            "blocks": [
                {"type": "divider"},
                {"type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": "*{}*".format(message["subject"])
                    }
                },
                {"type": "section", 'fields': header_fields},
                {"type": "divider"}
            ]
        }
        if self.slack and "channel" in self.slack:
            # Trim off the prefix of @ or # if it has one
            if self.slack["channel"].startswith("@") or self.slack["channel"].startswith("#"):
                self.slack["channel"] = self.slack["channel"][1:]
            request_payload["channel"] = self.slack["channel"]

        if "body_fields" in message:
            fields = []
            # Loop throuth the main sections (users, groups, and datasets)
            for section in message["body_fields"]:
                request_payload['blocks'].append({"type": "divider"})
                request_payload['blocks'].append(
                    {"type": "section", "text": {"type": "mrkdwn", "text": "*{}* ({})".format(section["title"].title(), message["action"])}}
                )
                request_payload['blocks'].append({"type": "divider"})
                # Loop through the section tasks
                for task in section["tasks"]:
                    fields = []
                    # Loop through the task fields
                    for field in task["fields"]:
                        fields.append({'type': 'mrkdwn', 'text': "*{}:* {}".format(field["label"], field["value"])})

                    request_payload['blocks'].append({
                        'type': "section",
                        'block_id': task["id"],
                        'fields': fields
                    })

        request_payload['blocks'].append({"type": "divider"})
        # from pprint import pprint
        # print("MESSAGE")
        # pprint(message)
        # print("PAYLOAD")
        # pprint(request_payload)

        if not self.slack or not "webhook" in self.slack:
            self.logger.error("No Slack configuration found. Unable to send Slack notification")
        else:
            resp = requests.post("{}".format(self.slack["webhook"]), timeout=15, verify=False, headers=headers, data=json.dumps(request_payload), allow_redirects=True)
            if not resp.ok:
                self.logger.error("Failed to send Slack notifaction: %s", str(resp.content))

        return resp.ok