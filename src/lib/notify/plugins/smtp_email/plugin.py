import os
plugin = {
    "name": "email",
    "version": "0.1.0",
    "description": "An SMTP Notification Plugin for FastAPI",
    "author": "Russ Cook",
    "vars": {
        "smtp_sender": os.environ.get("SMTP_FROM", "noreply@localhost"),
        "smtp_host": os.environ.get("SMTP_HOST", "localhost"),
        "smtp_port": os.environ.get("SMTP_PORT", 25),
        "smtp_username": os.environ.get("SMTP_USERNAME", None),
        "smtp_password": os.environ.get("SMTP_PASSWORD", None),
        "smtp_use_tls": os.environ.get("SMTP_USE_TLS", False),
        "smtp_use_ssl": os.environ.get("SMTP_USE_SSL", False)
    }

}