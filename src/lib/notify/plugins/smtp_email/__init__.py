import os
import yaml
import smtplib
from email.message import EmailMessage
from jinja2 import Environment, FileSystemLoader

class NotificationPlugin(object):
    def __init__(self, logger, **backend_config):
        self.name = "email"
        self._template_dir = None
        self.smtp = None
        self.host = None
        self.port = 25
        self.username = None
        self.password = None
        self.use_tls = False
        self.use_ssl = False
        self.templates = {}
        self.sender = None
        self.is_connected = False
        self.logger = logger
        for key, value in backend_config.items():
            if not isinstance(value, bool) and value.lower() in ["true", "1", "false", "0"]:
                value = bool(value.lower())
            self.logger.debug(f"plugin: {self.name} - Setting {key} = {value}")
            setattr(self, key.replace("smtp_", ""), value)

    @property
    def template_dir(self):
        return self._template_dir
    
    @template_dir.setter
    def template_dir(self, value:str):
        self._template_dir = os.path.realpath(f"{value}{os.sep}{self.name}")
        # A default template config
        template = {
            "name": "no-name",
            "description": "un-named notification template",
            "subject": "Job Notification"
        }
        
        if not os.path.isdir(self._template_dir):
            raise ValueError(f"Template directory: \"{self._template_dir}\" does not exist")
        
        for tmpl_dir in os.listdir(self._template_dir):
            tmpl_dir = f"{self._template_dir}{os.sep}{tmpl_dir}"
            tmpl_cfg = f"{tmpl_dir}{os.sep}template.yaml"

            if os.path.isdir(tmpl_dir) and os.path.isfile(tmpl_cfg):
                self.logger.debug(f"Found template dir: {tmpl_dir}")
                self.logger.debug(f"Found template cfg: {tmpl_cfg}")

                with open(tmpl_cfg) as file:
                    try:
                        template = yaml.safe_load(file)
                        template["directory"] = tmpl_dir
                    except yaml.YAMLError as err:
                        self.logger.error(f"Failed to load template config file: {tmpl_cfg}")
                        self.logger.error(err)
                        continue
                
                if not "files" in template:
                    self.logger.error(f"Template: {template['name']} does not contain any files. skipping template.")
                    continue
                for file_type, file in template["files"].items():
                    template["files"][file_type] = f"{tmpl_dir}{os.sep}{file}"
                    if not os.path.isfile(template["files"][file_type]):
                        self.logger.error(f"Template {template['name']} file: {file} does not exist. skipping template")
                
                self.templates[template["job_type"]] = template

        from pprint import pprint
        pprint(self.templates)

    def connect(self):
        """ Connect to the SMTP Server """
        if self.host:
            self.logger.debug("smtplib - Connecting to SMTP Server: %s:%s", self.host, self.port)
            if self.use_ssl or self.port == 465:
                self.logger.debug("smtplib - Starting SSL SMTP Connection")
                self.smtp = smtplib.SMTP_SSL(self.host, self.port)
            else:
                self.smtp = smtplib.SMTP(self.host, self.port)
            if self.use_tls:
                self.logger.debug("smtplib - Starting TLS")
                self.smtp.starttls()
            if self.username and self.password:
                self.logger.debug("smtplib - Authenticating to SMTP Server")
                self.smtp.login(self.username, self.password)
            
            self.is_connected = True

    def send(self, job_dict: dict, job_type="example") -> bool:
        from pprint import pprint
        print("notificatin:")
        pprint(job_dict)

        if not self.is_connected:
            self.connect()
    
        self.SendTemplatedMessage(
            template_name=job_type,
            sender=self.sender,
            recipients=job_dict["payload"]["notify"]["email"]["recipients"],
            data={"header_fields": job_dict["metadata"], "body_fields": job_dict["job_log"], "app": job_dict["app"]}
        )

    def SendTemplatedMessage(self, template_name, sender, recipients=[], data={}):
        """ Send a templated email message """
        # Verify SMTP Server is connected
        if not self.smtp:
            raise ValueError("SMTP Server not connected.")
        self.logger.debug("Sending SMTP email notification to: %s", ", ".join(recipients))

        data["subject"] = "TODO: Add a subject variable"

        # Verify that the template exists
        if not template_name in self.templates:
            raise ValueError(f"Template not found in configuration: {template_name}")

        if not os.path.isfile(self.templates[template_name]["files"]["html"]):
            raise ValueError(f"Template file not found: {self.templates[template_name]['html']}")
        
        # Load the jinja templates
        env = Environment(loader=FileSystemLoader(self.templates[template_name]["directory"]))
        
        template = env.get_template(os.path.basename(self.templates[template_name]["files"]["text"]))
        message_body_text = template.render(recipients=recipients, email_from=self.sender, data=data)

        template = env.get_template(os.path.basename(self.templates[template_name]["files"]["html"]))
        message_body_html = template.render(recipients=recipients, email_from=self.sender, data=data)


        # Send the message
        msg = EmailMessage()
        msg['Subject'] = self.templates[template_name]["subject"]
        msg['From'] = sender
        msg['To'] = ", ".join(recipients)
        msg.set_content(message_body_text)
        msg.add_alternative(message_body_html, subtype='html')

        return self.smtp.send_message(msg)