import os
import logging
from .._functions import *
from .._classes import *
from .plugins import available_plugins

class Notify():
    def __init__(self, template_dir) -> None:
        self.logger = logging.getLogger("uvicorn")
        self.template_dir = template_dir
        self.plugins = available_plugins

        # Add the template_dir to each plugin
        for plugin_name, plugin in available_plugins.items():
            if hasattr(plugin.module, "template_dir"):
                self.logger.debug(f"Adding template dir: {template_dir}{os.sep}notify to notification plugin: {plugin.name}")
                plugin.module.template_dir = f"{self.template_dir}{os.sep}notify"

    def send(self, job_object, app_config):
        self.logger.info(f"Received notifications for job: {job_object.id}")
        for plugin_name, plugin_payload in job_object.payload["notify"].items():
            if not plugin_name in available_plugins:
                self.logger.error(f"{plugin_name} is not a valid notification plugin.")
                continue
            
            self.logger.info(f"Sending notification with plugin: {plugin_name} for job: {job_object.id}")
            job_dict = job_object.to_dict()
            job_dict["app"] = app_config.info()
            available_plugins[plugin_name].module.send(
                job_dict=job_dict,
                job_type=job_object.type
            )