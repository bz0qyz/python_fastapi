import json

class SecretString(str):
    SECRET_KEYS = ["password", "passwd", "secret", "token", "key", "apikey", "barer"]
    """ Secret String """
    def __init__(self, value: str) -> None:
        self.secret = value
    def __repr__(self) -> str:
        return "************"
    def __str__(self) -> str:
        return "************"


class QueryFilter():
    """ Filter class for querying JobLogs """
    def __init__(self, field: str, values: list, operator: str) -> None:
        self.field = field
        self.values = values
        self.operator = operator

        if field.find(".") > 0:
            self.field = field.split(".")
        else:
            self.field = [field]

    def __str__(self) -> str:
        return f"{self.field} {self.operator} {self.values}"

