class Errors():
    INVALID_JSON = "Invalid json payload. Please check your request body."
    INVALID_JOB_ID = "Invalid job id. Please check your request id."
    JOB_NOT_FOUND = "Job not found. Please check your request id."
    JOB_IN_QUEUE = "Job is currently in the queue. Please wait for it to complete."
    
    def __init__(self) -> None:
        pass

    def error(self, error_code: str, message: str=None) -> dict:
        error_dict = {
            "error": error_code,
        }
        if message:
            error_dict["message"] = message
        return error_dict