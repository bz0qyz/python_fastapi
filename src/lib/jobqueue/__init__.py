import uuid
import threading
import ctypes
import logging
import time
from copy import deepcopy
from typing import Dict, List, Optional
from ..notify import Notify
from .._functions import *
from .._classes import *
from .plugins import available_plugins


class JobQueue():
    def __init__(self, plugin="pyqueue") -> None:
        self.logger = logging.getLogger("uvicorn")
        self.in_queue = []

        # Load the engine/plugin
        if not plugin in available_plugins:
            self.logger.warning(f"Queue engine: '{plugin}' not found. Using default plugin: 'pickledb'")
            plugin = "pyqueue"
        self.plugin = available_plugins[plugin].module
        self.logger.debug(f"Using the '{self.plugin.name}' engine for JobQueue")
    
    def __put_job_in_queue(self, job_object) -> bool:
        if job_object and self.plugin.put(job_object.id):
            self.logger.info(f"Job {job_object.id} added to queue")
            self.in_queue.append(job_object.id)
            job_object.enqueue()
            return True
        else:
            self.logger.error(f"Job {job_object.id} failed to be added to queue")
            job_object.status = Job.STATUS_FAILED
            job_object.attempt_log.append({"error": "Failed to add job to queue", "time": get_timestamp_str()})
            return False

    def insert(self, job_object) -> bool:
        return self.__put_job_in_queue(job_object)

    def put(self, job_payload: Dict, job_type: str="job"):
        # Create a new job object
        job_object = Job(payload=job_payload, job_type=job_type)
        if self.__put_job_in_queue(job_object):
            return job_object
        else:
            return None

    def get(self) -> Optional[Dict]:
        return self.plugin.get()
    
    def task_done(self):
        self.plugin.task_done()

    def delete(self, job_id: str) -> int:
        self.logger.debug(f"** Deleting job {job_id}")
        num_deleted = self.plugin.delete(job_id)
        self.logger.debug(f"** Deleted {num_deleted} jobs")
        if num_deleted > 0:
            self.in_queue.remove(job_id)
            return num_deleted
        else:
            return 0
    
    def purge(self) -> list:
        """
        Purge the queue of all jobs and return a list of the purged job IDs
        Returns: List of purged job IDs
        """
        return self.plugin.purge()
    
    def size(self) -> dict:
        return {
            "total": self.plugin.size(),
            "in_progress": self.plugin.in_progress()
        }
    
    def in_progress(self) -> int:
        return self.plugin.in_progress()


class Job():
    STATUS_NEW = "NEW"
    STATUS_RUNNING = "RUNNING"
    STATUS_COMPLETED = "COMPLETED"
    STATUS_FAILED = "FAILED"
    STATUS_CANCELLED = "CANCELLED"
    STATUS_RETRY = "RETRY_QUEUE"
    STATUS_QUEUED = "QUEUED"
    
    def __init__(self, payload:dict=None, job_type:str=None) -> None:
        self.logger = logging.getLogger("uvicorn")
        self.id = str(uuid.uuid4())
        self.type = job_type
        self.attempts_count = 0
        self.attempt_log = []
        self.job_log = []
        self.status = "NEW"
        self.timestamp = get_timestamp()
        self.time_created = get_timestamp_str()
        self.time_started = None
        self.time_completed = None
        self.completed = False
        self.payload = self.__munge_payload__(payload) if payload else None

    @property
    def status(self):
        return self._status
    def attemtps(self):
        return self.attempts_count

    @status.setter
    def status(self, value):
        self._status = value.upper()

    def __munge_payload__(self, payload, munge=True) -> dict:
        secret_keys = SecretString.SECRET_KEYS
        payload = deepcopy(payload)
        if isinstance(payload, list):
            for item in payload:
                item = self.__munge_payload__(item, munge)
        if isinstance(payload, dict):
            for key, value in payload.items():
                if key in secret_keys and isinstance(value, str):
                    if not munge and isinstance(value, SecretString):
                        # un-munge the secret
                        payload[key] = value.secret
                    else:
                        # munge the secret
                        payload[key] = SecretString(value)
                if isinstance(value, dict):
                    payload[key] = self.__munge_payload__(value, munge)
        return payload

    def __str__(self) -> str:
        return f"Job {self.id} of type {self.type} created at {self.time_created}"
    
    def to_dict(self, munge=False) -> dict:
        return {
            "metadata": {
                "id": self.id,
                "type": self.type,
                "attempts": self.attempts_count,
                "status": self.status,
                "timestamp": self.timestamp,
                "time_created": self.time_created,
                "time_started": self.time_started,
                "time_completed": self.time_completed,
                "completed": self.completed
            },
            "job_log": self.job_log,
            "attempt_log": self.attempt_log,
            "payload": self.__munge_payload__(self.payload, munge)
        }
    
    def from_dict(self, job_dict: dict) -> None:
        metadata_transform = {
            "attempts": "attempts_count",
            "status": "_status",
        }
        if "metadata" in job_dict:
            for key, value in job_dict["metadata"].items():
                if key in metadata_transform:
                    key = metadata_transform[key]
                setattr(self, key, value)
        if "attempt_log" in job_dict:
            self.attempt_log = job_dict["attempt_log"]
        if "job_log" in job_dict:
            self.job_log = job_dict["job_log"]
        if "payload" in job_dict:
            self.payload = self.__munge_payload__(job_dict["payload"])


    def complete(self, status="completed", log:dict=None):
        self.logger.debug(f"Job {self.id} completed with status {status}")
        if log:
            self.add_attempt_log(log)
        self.status = status
        self.time_completed = get_timestamp_str()
        self.completed = True
    
    def start(self):
        self.status = Job.STATUS_RUNNING
        self.time_started = get_timestamp_str()
        self.attempts_count += 1
        self.completed = False
    
    def requeue(self):
        self.logger.debug(f"Job {self.id} requeued")
        self.status = Job.STATUS_QUEUED
        self.completed = False
        self.time_completed = None
        self.time_started = None
        self.attempts_count = 0
        self.attempt_log = []

    def enqueue(self):
        self.status = Job.STATUS_QUEUED
        self.completed = False
    
    def add_attempt_log(self, log:dict):
        if not "time" in log:
            log["time"] = get_timestamp_str()
        if not "attempt" in log:
            log["attempt"] = self.attempts_count
        self.attempt_log.append(log)

    def add_job_log(self, log:dict):
        if not "time" in log:
            log["time"] = get_timestamp_str()
        self.job_log.append(log)


class QueueWatch(threading.Thread):
    def __init__(self, app_config, notifier: Notify, job_queue: JobQueue, job_log) -> None:
        # super().__init__()
        threading.Thread.__init__(self)
        self.app_config = app_config
        self.notifier = notifier
        self.job_queue = job_queue
        self.joblog = job_log
        self.interval = self.app_config.queue_interval
        self.max_retry = self.app_config.max_retry
        self.logger = logging.getLogger("uvicorn")
        self._stop_event = threading.Event()

    def __queue_wait__(self, interval):
        # While sleeping, watch for a stop event
        for n in range(self.interval):
            if self._stop_event.is_set():
                self.logger.debug(f"thread: {self.get_id()} - QueueWatcher is shutting down...")
                break
            else:
                # self.logger.debug(f"thread: {self.get_id()} - QueueWatcher is sleeping")
                time.sleep(1)

    def enqueue(self, job_object: Job=None, job_id: str=None) -> None:
        jid = job_object.id if job_object else job_id
        if jid not in self.job_queue.in_queue:
            self.job_queue.in_queue.append(jid)
    
    def dequeue(self, job_object: Job=None, job_id: str=None) -> None:
        jid = job_object.id if job_object else job_id
        if jid in self.job_queue.in_queue:
            self.job_queue.in_queue.remove(jid)
    
    def run(self) -> None:
        # Start Watching the job queue
        while not self._stop_event.is_set():
            # self.logger.debug(f"thread: {self.get_id()} - QueueWatch: Checking for jobs")
            job_id = self.job_queue.get()
            
            if not job_id:
                # self.logger.debug(f"thread: {self.get_id()} - QueueWatch: No jobs found")
                self.__queue_wait__(self.interval)
                continue
            
            job_object = self.joblog.get(job_id)
            if job_id and not job_object:
                self.dequeue(job_id=job_id)
                self.job_queue.task_done()
                self.logger.warning(f"thread: {self.get_id()} - Queued job: {job_id} not found in job log. Skipping.")

            if job_object and job_object.attempts_count <= self.max_retry:
                job_object.start()
                self.joblog.update(job_object)
                self.logger.info(f"thread: {self.get_id()} - Job {job_object.id} is being processed. Attempt: {job_object.attempts_count}")
                # Add job to the in_queue list
                self.enqueue(job_object)
                try: 
                    job_status = job_queue_callback(job_object)
                    if job_status:
                        self.logger.info(f"thread: {self.get_id()} - Job {job_object.id} completed successfully")
                        job_object.complete(Job.STATUS_COMPLETED, {"info": "Job completed successfully"})
                        self.joblog.update(job_object)
                        self.job_queue.task_done()
                        # Remove job from the in_queue list
                        self.dequeue(job_object)

                except ValueError as e:
                    self.logger.warning(f"thread: {self.get_id()} - Job {job_object.id} failed on attempt: {job_object.attempts_count}")
                    job_object.status = Job.STATUS_RETRY
                    job_object.add_attempt_log({"error": "Job failed", "exception": str(e)})
                    self.joblog.update(job_object)
                    self.job_queue.put(job_object)
                
                # Send job notifications
                if "notify" in job_object.payload and len(job_object.payload["notify"]) > 0:
                    self.logger.info(f"thread: {self.get_id()} - Sending Notifications for job: {job_object.id}")
                    self.notifier.send(job_object, self.app_config)
            
            elif job_object and job_object.attempts_count > self.max_retry:
                self.logger.error(f"thread: {self.get_id()} - Job {job_id} failed after {self.max_retry} attempts")
                job_object.complete(Job.STATUS_FAILED, {"error": "Job failed after max attempts"})
                self.joblog.update(job_object)
                self.job_queue.task_done()
                # Remove job from the in_queue list
                self.dequeue(job_object)

            self.__queue_wait__(self.interval)
    
    def stop(self) -> None:
        self.logger.info("QueueWatch: Stopping")
        self._stop_event.set()

    def get_id(self):
        # returns id of the respective thread
        if hasattr(self, '_thread_id'):
            return self._thread_id
        for id, thread in threading._active.items():
            if thread is self:
                return id

    def raise_exception(self):
        thread_id = self.get_id()
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id,
              ctypes.py_object(SystemExit))
        if res > 1:
            ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0)
            self.logger.error(f"thread: {self.get_id()} - Exception occurred")
