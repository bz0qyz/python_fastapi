import queue
import logging
from typing import Dict, List, Optional

class JobQueuePlugin():
    
    def __init__(self):
        self.name = "pyqueue"
        self.queue = queue.Queue()
        self.logger = logging.getLogger("uvicorn")

    def put(self, job_id: str):
        if self.queue.full():
            return False
        self.queue.put(job_id)
        return True

    def get(self) -> Optional[Dict]:
        try:
            return self.queue.get(block=True, timeout=1)
        except queue.Empty:
            return None
        
    def delete(self, job_id: str) -> int:
        num_deleted = 0
        while True:
            try:
                queued = self.queue.get(block=False)
                if queued != job_id:
                    self.logger.debug(f"{self.name}: Re-queued {queued} after purge")
                    self.queue.put(queued)
                    self.queue.task_done()
                else:
                    self.logger.debug(f"{self.name}: Purged {queued} from queue")
                    num_deleted += 1
                    self.queue.task_done()
                    break
            except queue.Empty:
                break
        return num_deleted
    
    def task_done(self):
        self.queue.task_done()
    
    def purge(self):
        purged_ids = []
        while True:
            try:
                queued = self.queue.get(block=False)
                self.queue.task_done()
                purged_ids.append(queued)
            except queue.Empty:
                break
        return purged_ids

    def size(self) -> dict:
        return self.queue.qsize()
    
    def in_progress(self) -> int:
        return self.queue.unfinished_tasks