import os
import sys
import importlib

available_plugins = {}
plugins_dir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(plugins_dir)

class QueuePlugin():
    def __init__(self, **qargs) -> None:
        self.name = None
        self.version = None
        self.module = None
        for key, value in qargs.items():
            setattr(self, key, value)

for plugin in os.listdir(plugins_dir):
    plugin_dir = None
    plugin_name = plugin
    if os.path.isdir(f"{plugins_dir}{os.sep}{plugin}") and not plugin.startswith("__"):
        plugin_dir = f"{plugins_dir}{os.sep}{plugin}"
    
    if plugin_dir and os.path.isfile(f"{plugin_dir}{os.sep}plugin.py"):
        exec(open(f"{plugin_dir}{os.sep}plugin.py").read())
        plugin_object = QueuePlugin(**plugin)
        if plugin_object.name in available_plugins:
            raise Exception(f"Duplicate queue engine name: {plugin_object.name}")

        package = importlib.import_module(f"{plugin_name}")
        if not hasattr(package, "JobQueuePlugin"):
            raise Exception(f"Invalid plugin: {plugin_name}. Missing JobQueuePlugin class")
        plugin_object.module = package.JobQueuePlugin()
        # from pprint import pprint
        # pprint(dir(package))
        # print(package.__name__)
        # print(package.__package__)

        available_plugins[plugin_object.name] = plugin_object