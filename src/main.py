import os
import sys
import logging
import uvicorn
from fastapi import FastAPI, Response, Request, status
from fastapi.responses import RedirectResponse
from fastapi.openapi.utils import get_openapi
from fastapi.staticfiles import StaticFiles
from config import AppConfig
from lib._models import *
from lib.jobqueue import Job, JobQueue, QueueWatch
from lib.joblog import JobLog
from lib.notify import Notify
from lib._errors import Errors


#####################################################
# Variables
#####################################################
# Configurable variables are set in config.py


#####################################################
# Objects
#####################################################
config = AppConfig()
if config.dev_mode:
    config.log_level = "debug"
    config.queue_interval = 5
    config.log_retention_days = 1
    config.uvicorn_reload = True
    config.uvicorn_workers = 1
    
app = FastAPI()
Errors = Errors()
# Set in the @startup event
JobLogger = None
JobQueueMgr = None
QueueWatcher = None
Notifier = None
logger = None


#####################################################
# API Routes
#####################################################
@app.get("/", response_class=RedirectResponse, status_code=302, include_in_schema=False)
async def root():
    return "/redoc"

@app.get("/doc", response_class=RedirectResponse, status_code=302, include_in_schema=False)
async def docs():
    return "/docs"

@app.get("/smoke", status_code=status.HTTP_200_OK, include_in_schema=False)
async def smoke_test():
    return {"status": "alive"}

@app.get("/info", response_model=AppInfo, status_code=status.HTTP_200_OK)
async def app_info():
    status_checks = {
        "name": config.name,
        "version": config.__version__,
        "description": config.description,
        "log_level": config.log_level,
        "queue": {
            "jobs": JobQueueMgr.in_queue,
            "size": JobQueueMgr.size(),
        },
        "backends": {
            "queue": JobQueueMgr.plugin.name,
            "job_log": JobLogger.plugin.name
        }
    }
    return status_checks

#################################
# API Routes: JobQueue
#################################
@app.get("/jobs/job/{job_id}", response_model=JobItem)
async def get_job(job_id: str):
    return_not_found = Errors.error(404, Errors.JOB_NOT_FOUND)
    if job_id is None or not JobLogger.exists(job_id):
        return return_not_found
    
    job_object = JobLogger.get(job_id)
    return return_not_found if job_object is None else job_object

@app.post("/jobs/new", response_model=JobItem, status_code=status.HTTP_201_CREATED)
async def new_job(payload: JobPayload):
    logger.info("New Job POST request received")
    # Prune logs older than log_retention_days
    JobLogger.prune(hours=(config.log_retention_days * 24))

    new_job = JobQueueMgr.put(job_payload=payload.__dict__, job_type="example")
    if new_job:
        JobLogger.save(job_id=new_job.id, job_dict=new_job.to_dict())
        return new_job.to_dict(munge=True)

@app.get("/jobs/count", response_model=QueueSize)
async def get_queue_count():
    return JobQueueMgr.size()

@app.get("/jobs/queue", response_model=JobList)
async def get_all_jobs_in_queue():
    """ Get all jobs from the queue that are not completed """
    queue_total = len(JobQueueMgr.in_queue)
    queue_jobs = JobLogger.get_incomplete()
    return {"total": queue_total, "jobs": queue_jobs}

@app.put("/jobs/requeue/{job_id}", response_model=BasicResponse)
async def add_job_to_queue(job_id, response: Response):
    """ Add a job to the queue """
    job_object = JobLogger.get(job_id)
    if job_object is not None:
        logger.info(f"Re-queueing job {job_id}")
        job_object.requeue()
        JobLogger.save(job_id=job_id, job_dict=job_object.to_dict())
        if JobQueueMgr.insert(job_object=job_object):
            response.status_code = status.HTTP_200_OK
            return {"message": f"Job {job_id} added to queue."}
        else:
            response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return {"status": "error", "message": f"Job {job_id} could not be added to queue."}
    else:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"status": "error", "message": f"Job {job_id} not found."}

@app.delete("/jobs/job/{job_id}", response_model=BasicResponse)
async def delete_job_from_queue(job_id, response: Response):
    job_object = JobLogger.get(job_id)
    num_del = 0
    reason = None
    if job_object is not None:
        num_del = JobQueueMgr.delete(job_id)
    else:
        reason = "was not found in the job log"
    if num_del > 0:
        job_object = JobLogger.get(job_id)
        if job_object is not None:
            job_object.complete(status=Job.STATUS_CANCELLED, log={"message": "Job cancelled by user"})
            response.status_code = status.HTTP_200_OK
            return {"status": "success", "message": f"Job {job_id} deleted from queue."}
    else:
        response.status_code = status.HTTP_404_NOT_FOUND
        if not reason:
            reason = "was either not in queue or is currently running"
        return {"status": "error", "message": f"Job {job_id} {reason}."}

@app.delete("/jobs/purge", response_model=BasicResponse)
async def delete_all_jobs_in_queue():
    """ Delete all jobs from the queue and the log (That are not completed) """
    purged_ids = JobQueueMgr.purge()
    for job_id in purged_ids:
        JobLogger.delete(job_id)
    return {"status": "success", "message": "All Jobs Deleted from queue"}

#################################
# API Routes: JobLog
#################################
@app.get("/log/all", response_model=JobList)
async def get_all_jobs_from_log():
    """ Get all jobs from the log """
    return JobLogger.get_all(munge=True)

@app.get("/log/{job_id}", response_model=JobItem)
async def get_job_from_log(job_id: str):
    """ Get a specific job from the log """
    return_not_found = Errors.error(404, Errors.JOB_NOT_FOUND)
    if job_id is None or not JobLogger.exists(job_id):
        return return_not_found
    
    job_object = JobLogger.get(job_id)
    return return_not_found if job_object is None else job_object.to_dict(munge=True)

@app.delete("/log/all", include_in_schema=False)
async def delete_all_job_logs():
    """ Delete all jobs from the log """
    return JobLogger.prune(all=True, in_queue=JobQueueMgr.in_queue)

@app.delete("/log/hours/{hours}")
async def delete_jobs_by_hours_old(hours: int):
    """ Prune the log of all jobs older than the specified hours """
    logger.info(f"Jobs in queue: {JobQueueMgr.in_queue}")
    return JobLogger.prune(hours=hours, in_queue=JobQueueMgr.in_queue)

@app.delete("/log/{job_id}")
async def delete_jobs_by_id(job_id: str, response: Response):
    """ Prune the log of a specified job id """
    if job_id is None or not JobLogger.exists(job_id):
        response.status_code = status.HTTP_404_NOT_FOUND
        return Errors.error(404, Errors.JOB_NOT_FOUND)
    if job_id in JobQueueMgr.in_queue:
        response.status_code = status.HTTP_400_BAD_REQUEST
        return Errors.error(400, Errors.JOB_IN_QUEUE)
    
    response.status_code = status.HTTP_200_OK
    return JobLogger.prune(job_id=job_id)

#####################################################
# Functions
#####################################################
def job_delete(job_ids: list, queue: bool = True, log: bool = True):
    """ Cleanly delete a job from the queue and the log """
    for job_id in job_ids:
        JobQueueMgr.delete(job_id)
        JobLogger.delete(job_id)
    
def custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title=config.name,
        version=config.__version__,
        description=config.description,
        routes=app.routes,
    )
    openapi_schema["info"]["x-logo"] = {
        "url": "static/logo.svg",
        "backgroundColor": "#FFFFFF",
        "altText": f"{config.company} Logo",
    }
    app.openapi_schema = openapi_schema
    return app.openapi_schema

#####################################################
# Startup
#####################################################
@app.on_event('startup')
async def startup():
    global logger
    global JobQueueMgr
    global QueueWatcher
    global JobLogger
    global Errors
    global config

    logger = logging.getLogger("uvicorn")
    logger.setLevel(config.log_level.upper())
    app.logger = logger
    app.mount("/static", StaticFiles(directory=config.static_path), name="static")

    start_header = f"{'*' * (len(config.__str__()) + 14)}"
    logger.info(start_header)
    logger.info(f"****** {config} ******")
    logger.info(start_header)

    # Load the plugins
    Notifier = Notify(template_dir=config.template_path)
    JobQueueMgr = JobQueue(plugin=config.queue_engine)
    JobLogger = JobLog(plugin=config.log_engine)
    QueueWatcher = QueueWatch(
        app_config=config,
        notifier=Notifier,
        job_queue=JobQueueMgr,
        job_log=JobLogger
    )
    
    if config.log_level == "debug":
        logger.debug("** DEBUG mode **")
        logger.debug(f"DEV_MODE: {config.dev_mode}")

    logger.info(f"LOG_LEVEL: {config.log_level.upper()}")
    logger.info(f"UVICORN_PORT: {config.uvicorn_port}")
    logger.info(f"UVICORN_WORKERS: {config.uvicorn_workers}")
    logger.info(f"UVICORN_RELOAD: {config.uvicorn_reload}")
    logger.info(f"NOTIFY_PLUGINS: {','.join(Notifier.plugins.keys())}")
    logger.info(f"QUEUE_ENGINE: config: {config.queue_engine} loaded: {JobQueueMgr.plugin.name}")
    logger.info(f"LOG_ENGINE: config: {config.log_engine} loaded: {JobLogger.plugin.name}")
    logger.info(f"LOG_RETENTION_DAYS: {config.log_retention_days} days")
    logger.info(f"QUEUE_INTERVAL: {config.queue_interval} seconds")
    logger.info(f"MAX_RETRY: {config.max_retry}")
    logger.info(f"STATIC_ASSETS_DIR: {config.static_path}")
    logger.info(f"TEMPLATES_DIR: {config.template_path}")
    logger.info(start_header)

    # Add a custom OpenAPI schema
    logger.debug("Adding custom OpenAPI schema")
    app.openapi = custom_openapi

    # Prune logs older than log_retention_days
    JobLogger.prune(hours=(config.log_retention_days * 24))

    # Check the queue log for any jobs that were running when the server was last shutdown
    # and requeue them
    logger.info("Importing incomplete jobs into queue")
    for job_object in JobLogger.get_incomplete():
        JobQueueMgr.insert(job_object)

    # Start the queue watcher
    logger.info(f"Starting Queue Watcher with {config.queue_interval} second polling interval")
    QueueWatcher.start()

#####################################################
# Shutdown
#####################################################
@app.on_event('shutdown')
async def shutdown():
    logger.info("Shutting down Queue Watcher")
    QueueWatcher.stop()
    app.state.is_shutting_down = True

#####################################################
# Main
#####################################################
if __name__ == "__main__":
    print("Starting FastAPI server with {uvicorn_workers} workers...")
    server_config = {
        "workers": config.uvicorn_workers,
        "port": config.uvicorn_port,
        "log_level": config.log_level,
        "reload": config.uvicorn_reload,
        "log_config": f"{os.path.dirname(__file__)}/uvicorn_logger.json",
        "timeout_graceful_shutdown": 10
    }
    uvicorn.run("main:app", **server_config)
    
    
    
    

    