import os

class AppConfig():
    """ Base application configuration """
    def __init__(self, base_path=os.path.dirname(os.path.realpath(__file__))):
        self.base_path = base_path
        self.name = "Fast API Sample"
        self.description = "Sample FastAPI application"
        self.version = {"major":0, "minor":1, "patch":0}
        self.author = "Russ Cook <rcook@64byte.com>"
        self.author_url = "https://64byte.com"
        self.company = "64byte"
        self.__version__ = '.'.join(map(str, self.version.values()))

        # Global Variables
        self.queue_engine = os.environ.get("QUEUE_ENGINE", "pyqueue")
        self.log_engine   = os.environ.get("LOG_ENGINE", "pickledb")
        self.uvicorn_workers = os.environ.get("UVICORN_WORKERS", 4)
        self.uvicorn_reload = os.environ.get("UVICORN_RELOAD", "False")
        self.uvicorn_port = os.environ.get("UVICORN_PORT", 5000)
        self.queue_interval = os.environ.get("QUEUE_INTERVAL", 30)
        self.log_retention_days = os.environ.get("LOG_RETENTION_DAYS", 7)
        self.log_level = os.environ.get("LOG_LEVEL", "info")
        self.max_retry = os.environ.get("MAX_RETRY", 5)
        self.dev_mode = os.environ.get("DEV_MODE", False)
        self.static_path = os.environ.get("STATIC_ASSETS_DIR", os.path.join(self.base_path, '..',"static"))
        self.template_path = os.environ.get("TEMPLATES_DIR", os.path.join(self.base_path, '..',"templates"))

    def __repr__(self):
        return f"""
        {self.name} ver {self.__version__} - {self.description}
        """

    def __str__(self):
        return f"{self.description} ver {self.__version__}"
    
    def info(self):
        return {
            "name": self.name,
            "description": self.description,
            "version": self.__version__,
            "copyright": self.company
        }